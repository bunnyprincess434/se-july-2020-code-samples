import { combineReducers } from "redux";
import counterReducer from "./counter-simple";
import counterComplexReducer from "./counter";

/*

{
  counter: 0,
  complexCounter: 0,
}

*/

const reducer = combineReducers({
  counter: counterReducer,
  complexCounter: counterComplexReducer,
});

export default reducer;
