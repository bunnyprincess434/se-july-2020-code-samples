import { Component } from "react";
import { connect } from "react-redux";
import { actions as counterActions } from "../redux/actions/counter";

class ReduxExample extends Component {
  state = {
    num: 0,
  };
  multiplyBy = () => {
    this.props.multiply(this.state.num);
  };
  render() {
    // console.log(this.props);
    const { increment, decrement, complexCounter } = this.props;
    const { num } = this.state;
    return (
      <>
        <h1>ConnectRedux</h1>
        <p>Props Number --> {complexCounter}</p>
        <p>State Number --> {num}</p>
        <input
          type="number"
          value={num}
          onChange={(e) => this.setState({ num: Number.parseInt(e.target.value, 10) })}
        />
        <button onClick={increment}>Increment</button>
        <button onClick={decrement}>Decrement</button>
        <button onClick={this.multiplyBy}>Multiply By My Input</button>
      </>
    );
  }
}

// how to get data FROM our redux store
const mapStateToProps = (state) => {
  return {
    complexCounter: state.complexCounter,
  };
};

const enhanceCounter = connect(mapStateToProps, counterActions);

export const ConnectRedux = enhanceCounter(ReduxExample);

// const ReduxExampleFunc = ({ increment, decrement, complexCounter }) => {
//   // const { increment, decrement, complexCounter } = props;
//   return (
//     <>
//       <h1>ConnectRedux</h1>
//       <p>{complexCounter}</p>
//       <button onClick={increment}>Increment</button>
//       <button onClick={decrement}>Decrement</button>
//     </>
//   );
// };

// how to dispatch actions TO our redux store
// const mapDispatchToProps = (dispatch) => {
//   return {
//     increment: () => dispatch({ type: "COUNTER_INCREMENT" }),
//     decrement: () => dispatch({ type: "COUNTER_DECREMENT" }),
//   };
// };
