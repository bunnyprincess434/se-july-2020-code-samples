import { Container } from "react-bootstrap";
import { Switch, Route, Redirect } from "react-router-dom";
import { Pokemon } from "./components/Pokemon";
import { ClassesExample, Species } from "./classes-example";
import { HooksExample, Starships } from "./hooks-example";
import Nav from "./Nav";
import { Counter } from "./Counter";
import { Example } from "./Example";

function App() {
  return (
    <Container>
      <Nav />
      <Switch>
        <Route path="/" exact>
          <Redirect to="/classes-example" />
        </Route>
        <Route path="/classes-example">
          <ClassesExample name="School" />
        </Route>
        <Route path="/hooks-example">
          <HooksExample name="Slim Shady" />
        </Route>
        <Route path="/counter">
          <Counter />
        </Route>
        <Route path="/species">
          <Species />
        </Route>
        <Route path="/starships">
          <Starships />
        </Route>
        <Route path="/pokemon">
          <Pokemon />
        </Route>
        <Route path="/example">
          <Example />
        </Route>
        <Route path="*">
          <h1>404 Bruh</h1>
        </Route>
      </Switch>
    </Container>
  );
}

export default App;
