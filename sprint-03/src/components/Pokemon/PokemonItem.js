import { useState, useEffect } from "react";
import { Card } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { pokemonApi } from "../../util";

export const PokemonItem = () => {
  const [pokemon, setPokemon] = useState(null);
  const params = useParams();
  useEffect(() => {
    async function fetchPokemon() {
      try {
        const result = await pokemonApi.findOne(params.pokemonId);
        setPokemon(result);
      } catch (err) {
        console.error(err);
      }
    }
    fetchPokemon();
  }, [params.pokemonId]);

  if (!pokemon) {
    return null;
  }

  return (
    <Card style={{ width: "18rem" }}>
      <Card.Img variant="top" src={pokemon.sprites.front_default} />
      <Card.Body>
        <Card.Title>{pokemon.name}</Card.Title>
        {/* <Card.Text>
          Some quick example text to build on the card title and make up the bulk of the card's content.
        </Card.Text> */}
      </Card.Body>
    </Card>
  );
};
