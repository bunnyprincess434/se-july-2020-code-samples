import express from "express";
import { v4 } from "uuid";

const weThePeople = [
  {
    id: "1",
    name: "Rochelle",
  },
  {
    id: "2",
    name: "Terry",
  },
];

const app = express();
//middleware from express that allows us to parse incoming JSON
app.use(express.json());

app.get("/", (req, res, next) => {
  console.log("Hello World!");
  res.json("Hi from our server =]");
});

app.get("/us", (req, res, next) => {
  res.status(200).json(weThePeople);
});

app.post("/us", (req, res, next) => {
  const entity = {
    id: v4(),
    name: req.body.name,
  };
  weThePeople.push(entity);
  res.status(201).json(entity);
});

app.put("/us/:id", (req, res, next) => {
  const entityIndex = weThePeople.findIndex((person) => person.id === req.params.id);
  if (entityIndex < 0) {
    res.status(404).json({
      message: "Person does not exist..",
    });
    return;
  }

  weThePeople[entityIndex] = {
    ...req.body,
    id: req.params.id,
  };

  res.status(204).json("ok!");
});

app.patch("/us/:id", (req, res, next) => {
  const entityIndex = weThePeople.findIndex((person) => person.id === req.params.id);
  if (entityIndex < 0) {
    res.status(404).json({
      message: "Person does not exist..",
    });
    return;
  }

  weThePeople[entityIndex] = {
    ...weThePeople[entityIndex],
    ...req.body,
    id: req.params.id,
  };

  res.status(204).json("ok!");
});

app.listen(4000, () => {
  console.info("Server successfully started on port 4000");
});
