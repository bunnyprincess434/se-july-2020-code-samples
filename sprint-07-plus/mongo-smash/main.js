import MongoClient from "mongodb";
import assert from "assert";

// Connection URL
const url = "mongodb://localhost:27017";

// Database Name
const dbName = "smash-tourney";

const insertDocuments = function (db, callback) {
  // Get the documents collection
  const collection = db.collection("participants");
  // Insert some documents
  collection.insertMany(
    [
      { name: "Captain Falcon", age: 74, fighter: true },
      { name: "Kirby", age: 24, fighter: true },
      { name: "Ganondwarf", age: 35, fighter: true },
    ],
    function (err, result) {
      assert.equal(err, null);
      assert.equal(3, result.result.n);
      assert.equal(3, result.ops.length);
      console.log("Inserted 3 documents into the collection");
      callback(result);
    }
  );
};

const findDocuments = function (db, callback) {
  // Get the documents collection
  const collection = db.collection("participants");
  // Find some documents
  collection.find().toArray(function (err, docs) {
    assert.equal(err, null);
    console.log("Found the following records");
    console.log(docs);
    callback(docs);
  });
};

// Use connect method to connect to the server
MongoClient.connect(url, function (err, client) {
  assert.equal(null, err);
  console.log("Connected successfully to server");

  const db = client.db(dbName);

  insertDocuments(db, () => {
    findDocuments(db, () => {
      client.close();
    });
  });
});
